package tp_shifumy;

public class Game {
    private static final int MAX_MATCH = 3;

    private final Player player1;
    private PlayerChoice p1_choice;

    private final Player player2;
    private PlayerChoice p2_choice;

    private int round = 0;
    private int winCursor = 0;

    public Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public int getRound() {
        return round;
    }

    public int getWinCursor() {
        return winCursor;
    }

    public boolean isFinish() {
        return round >= MAX_MATCH || winCursor == 2 || winCursor == -2;
    }

    public PlayerChoice player1Input() {
        do {
            p1_choice = (player1.isBot())
                    ? player1.getRandomChoice()
                    : player1.getChoice();
        } while(p1_choice == null);

        return p1_choice;
    }

    public PlayerChoice player2Input() {
        do {
            p2_choice = (player2.isBot())
                    ? player2.getRandomChoice()
                    : player2.getChoice();
        } while(p2_choice == null);

        return p2_choice;
    }

    public Player getWinner() {
        if(!isFinish()) return null;

        if(winCursor < 0) return player1;
        else if(winCursor > 0) return player2;

        return null;
    }

    public void turn() {
        if(p1_choice == null) player1Input();
        if(p2_choice == null) player2Input();

        if(p1_choice.ordinal() == ((p2_choice.ordinal() + 1) % 3))
            winCursor -= 1;
        else if(p2_choice.ordinal() == ((p1_choice.ordinal() + 1) % 3))
            winCursor += 1;

        p1_choice = null;
        p2_choice = null;

        round++;
    }
}
