package tp_shifumy;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

enum PlayerChoice {
    ROCK, PAPER, CHISEL
}

public class Player {
    private final String name;
    private final boolean isBot;
    private boolean displayChoices = true;

    public Player(String name, boolean isBot) {
        this.name = name;
        this.isBot = isBot;

        if(isBot) setDisplayChoices(false);
    }

    public String getName() {
        return name;
    }

    public boolean isBot() {
        return isBot;
    }

    public void setDisplayChoices(boolean displayChoices) {
        this.displayChoices = displayChoices;
    }

    public PlayerChoice getChoice() {
        int choice = 0;

        Scanner scan = new Scanner(System.in);
        if(this.displayChoices) {
            System.out.println("[" + name + "] Choose choice:");
            System.out.println(" 1 - ROCK");
            System.out.println(" 2 - PAPER");
            System.out.println(" 3 - CHISEL");
            System.out.print("> ");
        }
        choice = scan.nextInt() - 1;

        if(choice == PlayerChoice.ROCK.ordinal())
            return PlayerChoice.ROCK;
        else if(choice == PlayerChoice.PAPER.ordinal())
            return PlayerChoice.PAPER;
        else if(choice == PlayerChoice.CHISEL.ordinal())
            return PlayerChoice.CHISEL;

        return null;
    }

    public PlayerChoice getRandomChoice() {
        String choice_value = Integer.toString((int)Math.floor(Math.random()*(3-1+1)+1));

        InputStream sysInBackup = System.in;
        ByteArrayInputStream in = new ByteArrayInputStream(choice_value.getBytes());
        System.setIn(in);
        PlayerChoice choice = this.getChoice();
        System.setIn(sysInBackup);

        return choice;
    }
}
