package tp_shifumy;

public class App 
{
    public static void main( String[] args )
    {
        Player player1 = new Player("bot1", true);
        Player player2 = new Player("bot2", true);
        Game game = new Game(player1, player2);

        while(!game.isFinish())
            game.turn();

        Player winner = game.getWinner();

        if(winner != null)
            System.out.println("Player " + winner.getName() + " win");
        else
            System.out.println("Equality");
    }
}
