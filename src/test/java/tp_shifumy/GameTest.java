package tp_shifumy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest {
    private final InputStream sysInBackup;

    private Player player1;
    private Player player2;
    private Game game;

    private void simulateInput(String input) {
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }

    GameTest() {
        this.sysInBackup = System.in;
    }

    @BeforeEach
    void beforeEach() {
        System.setIn(this.sysInBackup);

        this.player1 = new Player("pseudo_bot1", false);
        this.player1.setDisplayChoices(false);

        this.player2 = new Player("pseudo_bot2", false);
        this.player2.setDisplayChoices(false);

        this.game = new Game(player1, player2);
    }

    @Test
    void should_be_valid_choice_for_player1() {
        this.simulateInput("1");
        PlayerChoice choice = game.player1Input();
        assertEquals(PlayerChoice.ROCK, choice);

        this.simulateInput("2");
        choice = game.player1Input();
        assertEquals(PlayerChoice.PAPER, choice);

        this.simulateInput("3");
        choice = game.player1Input();
        assertEquals(PlayerChoice.CHISEL, choice);
    }

    @Test
    void should_be_valid_choice_for_player2() {
        this.simulateInput("1");
        PlayerChoice choice = game.player2Input();
        assertEquals(PlayerChoice.ROCK, choice);

        this.simulateInput("2");
        choice = game.player2Input();
        assertEquals(PlayerChoice.PAPER, choice);

        this.simulateInput("3");
        choice = game.player2Input();
        assertEquals(PlayerChoice.CHISEL, choice);
    }

    @Test
    void should_be_player1_wins_the_round() {
        this.simulateInput("3");
        game.player1Input();

        this.simulateInput("2");
        game.player2Input();

        game.turn();

        assertEquals(1, game.getRound());
        assertEquals(-1, game.getWinCursor());
    }

    @Test
    void should_be_player2_wins_the_round() {
        this.simulateInput("1");
        game.player1Input();

        this.simulateInput("2");
        game.player2Input();

        game.turn();

        assertEquals(1, game.getRound());
        assertEquals(1, game.getWinCursor());
    }

    @Test
    void should_be_player2_winner() {
        while(!game.isFinish()) {
            this.simulateInput("1");
            game.player1Input();

            this.simulateInput("2");
            game.player2Input();

            game.turn();
        }

        assertEquals(player2, game.getWinner());
    }
}
