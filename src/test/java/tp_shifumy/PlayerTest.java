package tp_shifumy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {
    private final InputStream sysInBackup;

    private Player player;

    private void simulateInput(String input) {
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }

    PlayerTest() {
        this.sysInBackup = System.in;
    }

    @BeforeEach
    void beforeEach() {
        System.setIn(this.sysInBackup);
        this.player = new Player("john", true);
    }

    @Test
    void should_be_rock_choice() {
        this.simulateInput("1");
        PlayerChoice choice = this.player.getChoice();
        assertEquals(PlayerChoice.ROCK, choice);
    }

    @Test
    void should_be_paper_choice() {
        this.simulateInput("2");
        PlayerChoice choice = this.player.getChoice();
        assertEquals(PlayerChoice.PAPER, choice);
    }

    @Test
    void should_be_chisel_choice() {
        this.simulateInput("3");
        PlayerChoice choice = this.player.getChoice();
        assertEquals(PlayerChoice.CHISEL, choice);
    }

    @Test
    void should_be_impossible_choice() {
        this.simulateInput("5");
        PlayerChoice choice = this.player.getChoice();
        assertNull(choice);
    }

    @Test
    void should_be_not_null_choice() {
        for (int i = 0; i < 50; i++) {
            PlayerChoice choice = this.player.getRandomChoice();
            assertNotNull(choice);
        }
    }
}

